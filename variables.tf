variable "username" {
  description = "Username of the database"
  type        = string
}

variable "password" {
  description = "Password of the database"
  type        = string
}

variable "database_name" {
  description = "Name of the database"
  type        = string
}

variable "environment" {
  description = "Environment of the IAC"
  type        = string
}
