output "alb_dns_name" {
  description = "The DNS name of the application load balancer in timeoffec2"
  value       = module.login-infrastructure.alb_dns_name
}

output "url" {
  description = "The url of the dns server"
  value       = module.login-infrastructure.url
}
