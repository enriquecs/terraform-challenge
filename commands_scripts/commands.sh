#! /bin/bash

#uninstall old docker versions
sudo apt-get remove docker docker-engine docker.io containerd runc
#install docker 
sudo apt-get update -y 
sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release -y
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update -y 
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin -y 
#set up a directory
sudo mkdir -p /home/ubuntu/app
cd /home/ubuntu/app
#install Git and clone the repo
sudo apt-get install git -y
git clone -b backend https://gitlab.com/enriquecs/terraform-challenge.git
#build docker image
cd terraform-challenge
sudo docker build . -t node-app
#run docker container
sudo docker run -p 3000:3000 --env USER="${USER}" --env HOST="${HOST}" --env DATABASE="${DATABASE}" \
--env PASSWORD="${PASSWORD}" --env PORT="${PORT}" -d node-app