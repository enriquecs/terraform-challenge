const express = require('express')
const app = express()
var cors = require('cors');
app.use(cors());
const bodyParser = require('body-parser');
const { Pool } = require('pg')
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

const { config } = require('dotenv');
config();

const port = 3000
app.listen(port, () => {
})


app.get('/', (req, res) => {
   res.send('Hello World');
});

app.get('/auth', async (req, res) => {
   const {username, password} = req.query;
   try {
     const user = await db.query('SELECT * FROM users WHERE username = $1 AND password = $2', [username, password]);
     if (user.rows.length > 0) {
         res.send(`${user.rows[0].username} logged in`);
     } else {
         res.send('User not found');
     }
  } catch (error) {
     return console.log(error);
  }
});

app.get('/version', async (req, res) => {
   const {username, password} = req.query;
   try {
   const user = await db.query('SELECT * FROM users WHERE username = $1 AND password = $2', [username, password]);
   if (user.rows.length > 0) {
      res.send(`${user.rows[0].username} found, version running: ${user.rows[0].version}`);
     } else {
      res.send('User not found');
   }
  } catch (error) {
     return console.log(error);
  }
});

const db = new Pool({
   user: process.env.USER,
   host: process.env.HOST,
   database: process.env.DATABASE,
   password: process.env.PASSWORD,
   port: process.env.PORT,
})

db.connect(function(err) {
  if (err) throw err;
});

db.query(`create table users (
            username varchar(50) primary key not null,\ 
            password varchar(60) not null,\ 
            version varchar(50) not null);\
            
            insert into users (username, password, version)\
            values ('enriquecs', 'enriquecs', '${process.versions.node}');
            
            insert into users (username, password, version)\
            values ('user1', 'user1', '${process.versions.node}');
            `, 
            (err, res) => {
  console.log(err, res)
});



