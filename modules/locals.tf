
locals {

  sg_instance_1 = flatten([
    for sg_instance in var.instance_data.security_groups :
    aws_security_group.security_groups["${sg_instance}"].id
  ])

  //security groups
  list_of_rules = toset(flatten([
    for sg in var.security_groups : [
      for rules in sg.list_of_rules : [{
        name : rules.name
        type : rules.type
        description : rules.description,
        protocol : rules.protocol,
        from_port : rules.from_port,
        to_port : rules.to_port,
        cidr_blocks : rules.cidr_blocks,
        source_security_group_name : rules.source_security_group_name,
        security_group_name : sg.name
      }]
  ]]))

  //acm 
  acm_certificate = {
    name              = "${var.acm_certificate.name}_${var.environment}"
    domain_name       = join(".", [var.environment, data.aws_route53_zone.dns.name])
    validation_method = var.acm_certificate.validation_method

  }

  //route 53
  route53_record = {
    ttl     = var.acm_certificate.ttl
    zone_id = data.aws_route53_zone.dns.zone_id
  }

  route53_alias = {
    name    = join(".", [var.environment, data.aws_route53_zone.dns.name])
    type    = var.acm_certificate.route53_record_type
    zone_id = data.aws_route53_zone.dns.zone_id
    alias = {
      name                   = aws_lb.application-lb.dns_name
      zone_id                = aws_lb.application-lb.zone_id
      evaluate_target_health = var.acm_certificate.evaluate_target_health
    }
  }

  //load balancer
  load_balancer = {
    name               = var.load_balancer.name
    internal           = var.load_balancer.internal
    load_balancer_type = var.load_balancer.load_balancer_type
    security_groups_list = flatten([
      for sg_lb in var.load_balancer.security_groups :
      aws_security_group.security_groups["${sg_lb}"].id
    ])
    subnets_list = flatten([
      for subnet in var.load_balancer.subnets :
      aws_subnet.subnets["${subnet}"].id
    ])
  }

  lb_target_group = {
    name        = "${var.load_balancer.lb_target_group.name}-${var.environment}"
    port        = var.load_balancer.lb_target_group.port
    vpc_id      = aws_vpc.vpcs.id
    protocol    = var.load_balancer.lb_target_group.protocol
    target_type = var.load_balancer.lb_target_group.target_type
    health_check = {
      enabled  = var.load_balancer.lb_target_group.health_check.enable
      path     = var.load_balancer.lb_target_group.health_check.path
      interval = var.load_balancer.lb_target_group.health_check.interval
      port     = var.load_balancer.lb_target_group.health_check.port
      protocol = var.load_balancer.lb_target_group.health_check.protocol
      matcher  = var.load_balancer.lb_target_group.health_check.matcher
    }
  }

  lb_listener = flatten([
    for listener in var.load_balancer.lb_listener : [{
      name : listener.name
      port : listener.port,
      protocol : listener.protocol,
      default_action_type : listener.default_action_type,
      target_group_arn : listener.target_group_arn != null ? aws_lb_target_group.app-lb-tg.arn : null,
      ssl_policy : listener.ssl_policy != null ? listener.ssl_policy : null,
      certificate_arn : listener.certificate_arn != null ? aws_acm_certificate.lb-https.arn : null,
      redirect : listener.redirect
    }]
  ])

  lb_target_group_attachment = {
    // target_id = aws_instance.instance1.private_ip
    port = var.load_balancer.lb_target_group_attachment.port
  }


  //rds
  sg_rdb = flatten([
    for sg_rdb in var.rdb.security_group_ids :
    aws_security_group.security_groups["${sg_rdb}"].id
  ])

  subnet_group_ids = flatten([
    for subnet_name in var.subnet_group.subnet_ids :
    aws_subnet.subnets["${subnet_name}"].id
  ])

}