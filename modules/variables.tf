variable "vpc" {
  description = "VPC CIDR block"
  type = object({
    cidr_block   = string
    dns_support  = bool
    dns_hostname = bool
  })
}


variable "environment" {
  description = "Environment of the IAC"
  type        = string
}

variable "security_groups" {
  description = "Security groups of the IAC"
  type = list(object({
    name        = string
    description = string
    list_of_rules = list(object({
      name                       = string
      description                = string
      protocol                   = string
      from_port                  = number
      to_port                    = number
      cidr_blocks                = list(string)
      source_security_group_name = string
      type                       = string
    }))
  }))

}

variable "instance_data" {
  description = "values for the instance 1"
  type = object({
    name                        = string
    security_groups             = list(string)
    associate_public_ip_address = bool
    ami_name                    = string
    subnet                      = list(string)
    instance_type               = string
    count                       = number
    user_data                   = string
    key_name                    = string
    region                      = string
  })
}


variable "subnets" {
  description = "Subnets of the IAC"
  type = list(object({
    name              = string
    availability_zone = string
    cidr_block        = string
  }))
}

variable "load_balancer" {
  description = "Load balancer of the IAC"
  type = object({
    name               = string
    internal           = bool
    load_balancer_type = string
    subnets            = list(string)
    security_groups    = list(string)
    lb_target_group = object({
      name        = string
      port        = string
      vpc_id      = string
      protocol    = string
      target_type = string
      health_check = object({
        enable   = bool
        path     = string
        interval = number
        port     = number
        protocol = string
        matcher  = string
      })
    })
    lb_listener = list(object({
      name                = string
      port                = string
      protocol            = string
      default_action_type = string
      target_group_arn    = string
      ssl_policy          = string
      certificate_arn     = string
      redirect = list(object({
        status_code = string
        port        = number
        protocol    = string
      }))
    }))
    lb_target_group_attachment = object({
      port = number
    })
  })
}

variable "acm_certificate" {
  description = "ACM of the IAC"
  type = object({
    name                   = string
    dns_name               = string
    validation_method      = string
    route53_record_type    = string
    ttl                    = number
    evaluate_target_health = bool
  })
}

variable "rdb" {
  description = "RDB of the IAC"
  type = object({
    allocated_storage    = number
    db_name              = string
    engine               = string
    engine_version       = string
    port                 = number
    instance_class       = string
    username             = string
    password             = string
    parameter_group_name = string
    skip_final_snapshot  = bool
    security_group_ids   = list(string)
    db_subnet_group_name = string
  })

}

variable "subnet_group" {
  description = "Subnet group of the RDS"
  type = object({
    name        = string
    description = string
    subnet_ids  = list(string)
  })
}


variable "devops_name" {
  description = "Name of the devops"
  type        = string
}

variable "project_name" {
  description = "Project name"
  type        = string
}
