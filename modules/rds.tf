
resource "aws_db_instance" "dblogin" {
  allocated_storage      = var.rdb.allocated_storage
  db_name                = "${var.rdb.db_name}_${var.environment}"
  identifier             = "${var.rdb.db_name}-${var.environment}"
  engine                 = var.rdb.engine
  engine_version         = var.rdb.engine_version
  port                   = var.rdb.port
  instance_class         = var.rdb.instance_class
  username               = var.rdb.username
  password               = var.rdb.password
  parameter_group_name   = var.rdb.parameter_group_name
  skip_final_snapshot    = var.rdb.skip_final_snapshot
  vpc_security_group_ids = local.sg_rdb
  db_subnet_group_name   = aws_db_subnet_group.subnetgroup.name
  publicly_accessible    = true
  tags = {
    Name    = "${var.rdb.db_name}_${var.environment}"
    DevOps  = var.devops_name
    Project = var.project_name
  }

}