data "template_file" "arguments" {
  template = file("${var.instance_data.user_data}")
  vars = {
    USER     = var.rdb.username
    HOST     = aws_db_instance.dblogin.address
    DATABASE = "${var.rdb.db_name}_${var.environment}"
    PASSWORD = var.rdb.password
    PORT     = var.rdb.port
  }
}


resource "aws_instance" "instance1" {
  provider                    = aws
  ami                         = var.instance_data.ami_name
  instance_type               = var.instance_data.instance_type
  count                       = var.instance_data.count
  key_name                    = var.instance_data.key_name
  associate_public_ip_address = var.instance_data.associate_public_ip_address
  subnet_id                   = aws_subnet.subnets[var.instance_data.subnet[count.index]].id
  vpc_security_group_ids      = local.sg_instance_1
  user_data                   = data.template_file.arguments.rendered
  tags = {
    Name    = "${var.instance_data.name}_${count.index + 1}_${var.environment}"
    DevOps  = var.devops_name
    Project = var.project_name
  }

  depends_on = [aws_db_instance.dblogin]

}
