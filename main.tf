
module "login-infrastructure" {
  source = "./modules"

  providers = {
    aws = aws.main_region
  }

  environment = var.environment
  vpc = {
    cidr_block   = "10.0.0.0/16"
    dns_support  = true
    dns_hostname = true
  }
  subnets = [
    {
      name              = "subnet_1"
      availability_zone = "us-east-1a"
      cidr_block        = "10.0.1.0/24"
    },
    {
      name              = "subnet_2"
      availability_zone = "us-east-1b"
      cidr_block        = "10.0.2.0/24"
    },
    {
      name              = "subnet_3"
      availability_zone = "us-east-1c"
      cidr_block        = "10.0.3.0/24"
    },
    {
      name              = "subnet_4"
      availability_zone = "us-east-1d"
      cidr_block        = "10.0.4.0/24"
    },
  ]
  security_groups = [
    {
      name        = "sg_load_balancer_1"
      description = "Security group for load balancer"
      list_of_rules = [
        {
          name                       = "ingress_rule_1"
          description                = "Allow inbound trafic from HTTPS"
          protocol                   = "tcp"
          from_port                  = 443
          to_port                    = 443
          cidr_blocks                = ["0.0.0.0/0"]
          source_security_group_name = null
          type                       = "ingress"
        },
        {
          name                       = "ingress_rule_2"
          description                = "Allow inbound trafic from HTTP"
          protocol                   = "tcp"
          from_port                  = 80
          to_port                    = 80
          cidr_blocks                = ["0.0.0.0/0"]
          source_security_group_name = null
          type                       = "ingress"
        },
        {
          name                       = "egress_rule_1"
          description                = "Allow outbound trafic to anywhere"
          protocol                   = "-1"
          from_port                  = 0
          to_port                    = 0
          cidr_blocks                = ["0.0.0.0/0"]
          source_security_group_name = null
          type                       = "egress"
        }
      ]
    },
    {
      name        = "security_group_instances"
      description = "Security group for instances"
      list_of_rules = [
        {
          name                       = "ingress_rule_3"
          description                = "Allow 22 for our public IP"
          protocol                   = "tcp"
          from_port                  = 22
          to_port                    = 22
          cidr_blocks                = ["0.0.0.0/0"]
          source_security_group_name = null
          type                       = "ingress"
        },
        {
          name                       = "ingress_rule_4"
          description                = "Allow inbound trafic from HTTP"
          protocol                   = "tcp"
          from_port                  = 3000
          to_port                    = 3000
          cidr_blocks                = []
          source_security_group_name = "sg_load_balancer_1"
          type                       = "ingress"
        },
        {
          name                       = "egress_rule_2"
          description                = "Allow outbound trafic to anywhere"
          protocol                   = "-1"
          from_port                  = 0
          to_port                    = 0
          cidr_blocks                = ["0.0.0.0/0"]
          source_security_group_name = null
          type                       = "egress"
        }
      ]
    },
    {
      name        = "security_group_db"
      description = "Security group for db"
      list_of_rules = [
        {
          name                       = "ingress_rule_5"
          description                = "Allow PostgreSQL trafic from web sg"
          protocol                   = "tcp"
          from_port                  = 5432
          to_port                    = 5432
          cidr_blocks                = []
          source_security_group_name = "security_group_instances"
          type                       = "ingress"
        },
        {
          name                       = "egress_rule_3"
          description                = "Allow outbound trafic to anywhere"
          protocol                   = "-1"
          from_port                  = 0
          to_port                    = 0
          cidr_blocks                = ["0.0.0.0/0"]
          source_security_group_name = null
          type                       = "egress"
        }
      ]
    },
  ]


  load_balancer = {
    name               = "lb-1"
    internal           = false
    load_balancer_type = "application"
    subnets = [
      "subnet_1",
      "subnet_2"
    ]
    security_groups = [
      "sg_load_balancer_1"
    ]
    lb_target_group = {
      name        = "tg-1"
      port        = "3000"
      vpc_id      = "vpc_1"
      target_type = "instance"
      protocol    = "HTTP"
      health_check = {
        enable   = true
        path     = "/"
        interval = 10
        port     = 3000
        protocol = "HTTP"
        matcher  = "200-299"
      }
    }
    lb_listener = [
      {
        name                = "lb_l_1"
        port                = "80"
        protocol            = "HTTP"
        default_action_type = "redirect"
        target_group_arn    = null
        ssl_policy          = null
        certificate_arn     = null
        redirect = [
          {
            status_code = "HTTP_301"
            port        = 443
            protocol    = "HTTPS"
          }
        ]
      },
      {
        name                = "lb_l_2"
        port                = "443"
        protocol            = "HTTPS"
        default_action_type = "forward"
        target_group_arn    = "tg-1"
        ssl_policy          = "ELBSecurityPolicy-2016-08"
        certificate_arn     = "acm_1"
        redirect            = []
      },

    ]
    lb_target_group_attachment = {
      port = 3000
    }

  }

  instance_data = {
    name                        = "instance"
    security_groups             = ["security_group_instances"]
    subnet                      = ["subnet_1", "subnet_2"]
    instance_type               = "t3.micro"
    count                       = 2
    associate_public_ip_address = true
    ami_name                    = "ami-0149b2da6ceec4bb0"
    user_data                   = "commands_scripts/commands.sh"
    key_name                    = "key_pair_1"
    region                      = "us-east-1"
  }

  acm_certificate = {
    name                   = "acm_1"
    dns_name               = "opstestings.me."
    validation_method      = "DNS"
    route53_record_type    = "A"
    ttl                    = 60
    evaluate_target_health = true
  }

  rdb = {
    allocated_storage    = 10
    db_name              = var.database_name
    engine               = "postgres"
    engine_version       = "13.8"
    port                 = 5432
    instance_class       = "db.t3.micro"
    username             = var.username
    password             = var.password
    parameter_group_name = "default.postgres13"
    skip_final_snapshot  = true
    security_group_ids   = ["security_group_db"]
    db_subnet_group_name = "subnet_group_1"
  }

  subnet_group = {
    name        = "subnet_group_1"
    description = "Subnet group for db"
    subnet_ids  = ["subnet_3", "subnet_4"]
  }

  devops_name  = "Enrique Cruz"
  project_name = "Challenge"
}